# Rust Web Server

[![Developer](https://img.shields.io/badge/Developed_by-Nootaku_(MWattez)-informational?style=for-the-badge)](https://gitlab.com/Nootaku)<br/>
![Version](https://img.shields.io/badge/Version-1.0.0-yellow?style=for-the-badge)

The aim of this project is to follow up on a [CLI application]() and [Terminal Game]() that I made earlier in order to get started with Rust. Here I will create a web-server from scratch. My objective is not for my web-server to be as performant as possible, but to dig deeper into the Rust syntax and understand how the language works.

So this is a Rust web-server that can serve web-pages with their corresponding HTML and CSS files.

Below you will find the development notes of this project (for educational purposes).

## Objectives

1. Get familiar with the Rust syntax
2. Dig deeper

## Protocols and architecture

I used HTTP 1.1 for this server. This is an application layer (L7) protocol that is sent over TCP and message based (request / response).

Example request: <br/>
```
GET /my-path?optional=query-param HTTP/1.1
Accept: text/html
```

Example response:<br/>
```
HTTP/1.1 200 OK
Content-type: text/html

<!DOCTYPE html>
<html lang="en">
...
</html>
```

> **Note:**<br/>
Since my goal is to **learn Rust** and not to create a production-ready web-server, I simplified the protocol by ignoring headers in both the request and response.

### Architecture

1. TCP-listener: listen for new TCP connexions and read an write bytes from those connexions
2. HTTP parser: parse information in usefull HTTP data-structures
3. Handler: executes code based on routing logic, then will send a response to the TCP-listener

This server runs on a single thread. This means it can only handle one request at the time.

## Rust concepts

### Structs

Structs correspond to the `class` in other languages. However they are implemented differently.<br/>
A `struct` accepts only the parameters of a class but not the methods. To set methods for a `struct` you need to implement them with the `impl` keyword.

This might look like a pain in the neck, but it actually allows to implement external functions as well. Moreover, it allows to define the type of a variable based on implemented functions. This removes the need for class-inheritance and proves very efficient.

```rust
struct Animal {
	race: str,
	diet: str,
	legs: usize,
	fantastic: bool
}

impl Animal {
	pub fn new(race: str, diet: str, legs: usize, fantastic: bool) -> Self {
		Self {
			race: race,
			diet: diet,
			legs: legs,
			fantastic: fantastic,
		}
	}
}
```

#### Implementation blocks

Within an implementation block we can have two types of functionnality:
- methods
- associated function

**Methods**<br/>
A method is like a regular function but it is defined in the context of a `struct`. A method _always_ takes a first parameter called `self`. `self` represents the instance of the `struct` that the method is being called on.

**Associated functions**<br/>
An associated function is a function that is ... associated with a `struct` type (duh !) BUT they _do not need an instance of the `struct`_. This is the equivalent of a static function in other languages.

### Enums

Enums like in many other languages allow to specify allowed values for an argument. In it simplest form `enum` are writen:

```rust
enum MyEnum {
	option1,
	option2,
	option3,
}
```

However, `enum` is much more powerful than this in Rust. For starters, we can define give arguments to an `enum` specifying its type:
```rust
enum MyEnum {
	option1,
	option2(String),
	option3(usize),
	option4(bool),
}

// this would be the same as:
enum Thing {
	option1,
	option2,
}

struct Option2 {
	kind: Thing,
	value: String
}
```

Finally we can also give an option a default value instead of the integer it is associated with by default.

#### The `Option` enum and the `<T>`

One of the `enum` that is part of the standard library is the `Option` enum. The definition of this enum is:
```rust
pub enum Option<T> {
	None,
	Some(T)
}
```

Let's start with the `<T>`. This corresponds to the `any` type in other languages. But it has to be **explicitly** specified as part of the `Option` type.

Now, the `Option` type is used to create optional values - or rather, the possible absence of value. This is important because there is no `null` type in Rust.

#### The `Result` enum

The `Result` enum is very important in Rust and is at the core of the language error handling. The underlying idea is that _errors will happen_. And indicating how to deal with errors before they happen allows for a better quality of code.

Rust distinguishes two types of errors:
- recoverable: program should not crash if error is encountered
- unrecoverable: program will crash if error is encountered

Most languages handle those two categories of error in the same way using `Exceptions`. However Rust does not support `Exceptions` and uses the `Result` enum to recover from _recoverable errors_.

```rust
pub enum Result<T, E> {
	Ok(T),  // contains the success value
	Err(E), // contains the error value
}
```

To work with the `Result` enum we have multiple ways of handling the response.

If we want to convert a recoverable error in an unrecoverable error, we can use the `.unwrap()` method that only returns the `Ok` of a `Result`.

> Note:<br/>The `Result` can return custom types (incl. _tuples_).

### Modules

As the code base grows, we want to organize our code. It is important to first understand what a module is in a single `main.rs` file before splitting it in multiple files.

First we can create a module with the `mod` keyword. All the content of a module are, by default, private. This means that the visibility of the content needs to be explicitely set to public with the `pub` keyword.

```rust
mod server {
	pub struct WebServer {
	    addr: String,
	}

	impl WebServer {
	    pub fn new(addr: String) -> Self {
	        Self {
	            addr: addr,
	        }
	    }

	    // Method
	    pub fn run(self) {
	        println!("Web Server listening on {}", self.addr);
	    }
	}
}
```

The second thing we need to do is import the module inside the code of our `main.rs`. This is done by using `use server::WebServer;`.

Finally, if I have a module with submodules, then I need to also specify the visibility of the submodules compared to each other:
```rust
mod http {
	// method should be public because it is used by requests
	pub mod method {
		pub enum Method {
		    GET,
		    POST,
		    PUT,
		    PATCH,
		    DELETE,
		    HEAD,
		    CONNECT,
		    OPTIONS,
		    TRACE,
		}
	}

	// requests should be public becaus it is used in main.rs
	pub mod requests {

		// we need to import Method from 'one level higher' -> super
		use super::method::Method;

		pub struct Request {
		    path: String,
		    query: Option<String>,
		    method: Method,
		}
	}
}
```

#### Splitting in different files

Splitting the code in different files is actually pretty easy.

By default when we create new files in our `src` folder, each file is considered as a module. This means that I can create a `http.rs` file and dump the content of my `http` module. This will keep all the functionnality.

However if we were to import the `Request` struct in `main.rs` using `use http::requests::Request`, we would have an error. This is because we have not defined the module in `main.rs`. To do this we should:
```rust
use http::requests::Request;

mod http;
```

This strange behaviour is exacerbated when we use multiple files in multiple directories. When this happens, each directory should have a `mod.rs` file. The `mod.rs` file is similar to the `__init__.py` file in Python. It contains the list of submodules that we want to publically make available.

```rust
// The ./http/mod.rs file
pub use requests::Request;
pub use method::Method;

pub mod requests;
pub mod method;
```
```rust
// The ./main.rs file
use http::Request;
use http::Method;

mod http;

// and in the http/mod.rs file
pub mod method;
pub mod requests;
```

### Loops

We can create infinite using the `loop` keyword. This is equivalent to `while true`.<br/>
Just as in other languages, we can break out of a loop using the `break` keyword and go to the next iteration of the loop using the `continue` keyword.

Loops can also be named using the `'loopname: loop` syntax. This means we can break a specific loop using the `break 'loopname;` syntax.

### Match

The `match` keyword is the equivalent of a `switch` `case` in other languages. This is very important in Rust as it allows to parse through enums. The most important of which are the `Result` enums.

When using `match` _all the possibilities_ need to be accounted for. If we want to handle a _default_ (for multiple cases) we can use the `_` case.

The syntax goes as follows:
```rust
match my_function_that_will_return_a_match() {
	Ok(ok_return_value) => {
		// Do things with ok_return_value
	},
	Err(error_return_value) => {
		// Handle the error
	}
}
```

It is, of course, possible to nest `match` statements.

#### The `?` syntax

When we have a `Return` statement we almost always want to `match` it to handle errors:
```rust
match str::from_utf8(value).or(Err(ParseError::InvalidEncoding)) {
    Ok(request) => {},
    Err(err) => return Err(err),
}
```

But this syntax is so common that there is a special syntax that does just that:
```rust
let request = str::from_utf8(value).or(Err(ParseError::InvalidEncoding))?;
```

#### The `if let` statement

On the opposite side of the spectrum, we have the following match:
```rust
let mut my_string = "/foobar?param=0";

match my_string.find('?') {
            Some(i) => {
                query_string = Some(&my_string[i + 1..]);
                path = &my_string[..i];
            },
            None => {}
        }
```

We only really care about the `Some()` of the `Option`, not about the `None`. In other words, nothing should happen if the `.find()` method fails.

This is solved using the `if let` statement.
```rust
if let Some(i) = path.find('?') {
	query_string = Some(&my_string[i + 1..]);
    path = &my_string[..i];
}
``` 

### Traits

Traits are very similar to `interface` in other languages. It is an _abstract functionnality that needs to be manually implemented_ on the Type that needs it.

To implement a trait into a type we use the syntax:
```rust
impl TraitName for TypeName{
	// do the actual implementation
}
```

One really powerfull aspect of traits is that the implementation of a trait on a type _can_ automatically implement other traits on other types without us having to implement them.<br/>In my code, I use the `TryFrom` trait. This trait implicitely implements the `TryInto` trait for the source type (in my case `&[u8]`).

Moreover, traits also allow us to add functionality to types that we have not coded ourselves and / or that come from external crates.

For example, I could create a trait allowing me to encrypt all the bytes of a type and implement this trait to `String` or `TcpListener`.

### Lifetimes

In this project I wanted to optimize memory allocation.

I have a buffer containing a `String`. This buffer is saved in the HEAP. I could create a `path` and a `query` parameter for my `Request` that are of type `String` as well. This would save both parameters in the HEAP as well.

However, since `path` and `query` are derived from parsing the buffer, it is more memory efficient to store them on the stack using string slices (`str`).

But this leads to a new problem: What would happen if the buffer goes _out of scope_ while the `Request` is still _in scope_ ? This is called having **dangling references**.

This problem is solved by using _lifetimes_ in Rust. Lifetimes ensure that the compiler knows the lifetime of the value that a pointer is pointing into. This prevents dangling references.

> Please note that lifetimes are one of the hardest concepts of Rust to master as they don't have any equivalent in other languages.
> A lifetime is a type of _metadata_ that we give the compiler to indicate to him that some values are "related".

#### The syntax

To specify lifetimes in a `struct` we have to make the `struct` generic over a lifetime. Making a struct generic is done using the angle brackets (`<>`). But instead of inserting a type in those brackets we will insert a lifetime.

A lifetime is **usually defined by a single letter**. And identifying a lifetime uses the same syntax as identifying a `loop` - by using a `'` before its name.

```rust
// using the name buffer instead of single letter for readability

pub struct Resquest<'buffer> {
	path: &'buffer str,
    query: Option<&'buffer str>,
    method: Method,
}
```

If a `struct` has a lifetime, it means it is generic. This means we need to specify the lifetime in the `impl` block as well. However the `impl` block does know the lifetime that will be used. This is why we need to declare it on the `impl` keyword first.

```rust
impl<'buffer> TryFrom<&[u8]> for Request<'buffer> {
	// do stuff
}
```

### Vectors

A vector is a **heap allocated array**. This allows us to create an array without knowing its lenght at compile time.

```rust
let mut foo: Vec<&str> = ["meh", "bar"];
```

Please note that `Vec` is generic over a type (in this case `&str`).

### Dereferencing

Let's imagine we have some value that is in the Heap: `let mut my_vector<&str, &str> = Vec::new();`

Let's now imagine that we have a pointer to that reference that value: `my_vector.entry("foo").and_modify(|existing_value: &mut str| { do stuff });`

What would happen to the existing value if we try to modify it ?

It wouldn't work. That is why we need to dereference the value. In other words we don't want to act on the pointer but on the value itself.

This is done using the asterisk syntax: `*existing_value = "my new value";`

### The Derive Attribute

If we would want to use the `dbg!` macro on our server to see that everything works as expected, it could be a painful process. We would have to implement the `Debug` trait on all the types that we are using.

To help us out, the Rust compiler can provide basic implementation for some traits. `Debug` is one of them. This basic implementation of traits is done via the **derive** attribute.

```rust
#[derive(Debug)]
pub struct MyStruct {
	// struct
}
```

This syntax is similar to the attribute we can use to mute warnings:

```rust
#![allow(dead_code)]
```

But note that the hashtag is followed by a bang (`!`). This means that the attribute will be applied to the item it is declared within.

This means that if we place it at the top of our `main.rs` file, it will be applied to our main modules and all its sub-modules, or the entire project.

Without the bang, the attribute is only applied to the expression that follows it.


### Copying vs Cloning

Generally speaking, the values in our programs can be divided in two types: Values that live on the _stack_ and values that live on the _stack_ and in the _heap_.

The values that live entirely on the stack can easily be copied just by copying their bytes (since they have a defined number of bytes at compile time).

The values that live both on the stack an in the heap it is not the same. Copying the value on the stack for a String for example will only copy the pointer towards the heap allocated memory of the String. This is not good as we now have 2 pointers pointing at the same value. This goes against the ownership rules.

That's why we have *cloning* cloning allows to create a _deep copy_ on the heap of an allocated memory zone and the creation of a new pointer that points towards that copy.

In this program, the following code returned an error:
```rust
pub enum StatusCode {
	Ok = 200,
	BadRequest = 400,
	NotFound = 404,
	ServerError = 500
}


impl StatusCode {
	pub fn verbose_status(&self) -> &str {
		match self {
			Self::Ok => "Ok",
			Self::BadRequest => "Bad Request",
			Self::NotFound => "Not Found",
			Self::ServerError => "Server Error",
		}
	}
}


impl Display for StatusCode {
	fn fmt(&self, formatter: &mut Formatter) -> FmtResult {
		// https://doc.rust-lang.org/error_codes/E0606.html
		write!(formatter, "{}", *self as u16)
	}
}
```

The error is:
```
error: src/http/status_code.rs:27: cannot move out of `*self` which is behind a shared reference
error: src/http/status_code.rs:27: move occurs because `*self` has type `StatusCode`, which does not implement the `Copy` trait
```

To solve the issue we need to implement a `Copy` trait. But it is actually easy since we have a `derive` attribute for `copy`.

```rust
#[derive(Copy, Clone)]
pub enum StatusCode {
	Ok = 200,
	BadRequest = 400,
	NotFound = 404,
	ServerError = 500
}
```

---

## Implementation details

### WebServer

Creating the TCP socket and binding it to the address I wanted to use has been done using the `std` library ([documentation](https://doc.rust-lang.org/std/net/index.html))

Within that library we will use the `TcpListener` and bind it to a port on our localhost. Then we will use an infinite `loop` to listen for TCP requests.

Each request needs to be accepted using the `TcpListener.accept()` method. This method returns a `Result` which means we wrap it in a `match` to handle errors.


### Request

I chose to use an `enum` for my `Request.method` since it cannot be just any string. It must be a valid HTTP method. And those are limited.

How do I convert the stream of bytes to a Request object ?

The most obvious solution would be to create an implementation that converts a byte array into a Request:
```rust
impl Request {
	// we return a result because the function can fail (ex: invalid utf8 buffer)
	pub fn get_request(buffer: &[u8]) -> Result<Request, String> {
		// convert and return a Request object
	}
}
```

However there is a standard module dedicated to type conversions: [the convert module](https://doc.rust-lang.org/std/convert/index.html).

```rust
use std::convert::TryFrom;

impl TryFrom<&[u8]> for Request {
    type Error = String;

    fn try_from(value: &[u8]) -> Result<Self, Self::Error> {
        // Convert and return a Request object
    }
}
```

I wanted to optimize memory allocation.<br/>
For this reason I have decided that the `path` and `query` should be string slices pointing to the request buffer. This implies a reference to the lifetime of the buffer.

This means we have a structure:
```rust
// note: we borrow the str because it comes from the buffer

pub struct Request<'buffer> {
    path: &'buffer str,
    query: Option<&'buffer str>,
    method: Method,
}
```

This poses a new problem: In the `impl TryFrom`, there is a conflict between the lifetime `'buffer` and the lifetime of `value` (which corresponds to the buffer). Let's elaborate the error:

**Deep dive**

```rust
// 1. we have a buffer that we pass as a reference in the 'value' variable
//    buffer has a lifetime
fn try_from(value: &[u8]) -> Result<Self, Self::Error> {

	// 2. adding a pointer to the buffer
    let request = str::from_utf8(value)?;

    // 3. creating more pointers to buffer (method, path, protocol)
    let (method, request) = get_request_element(&request).ok_or(ParseError::InvalidRequest)?;
    let (mut path, request) = get_request_element(&request).ok_or(ParseError::InvalidRequest)?;
    let (protocol, _) = get_request_element(&request).ok_or(ParseError::InvalidRequest)?;
```

This means that path, method and protocol should have the same lifetime as the parameter we give the `get_request_element()` function

```rust
// 4. The compiler will implicitely give lifetimes to the references 'request' and '&str' of Option
fn get_request_element(request: &str) -> Option<(&str, &str)> {
    for (index, character) in request.chars().enumerate() {
        if character == ' ' || character == '\r' {
            return Some((&request[..index], &request[index + 1..]));
        }
    }
    None
}
```

By default the compiler knows that the lifetime of `&str` in the `Option` is the same as the one of `request`. However, what would happen if we had 2 parameters: `fn get_request_element(request: &str, foo: &str)` ?

Then the compiler wouldn't be able to infer which lifetime should be allocated to the return of the function. We should have to explicitely define it:
```rust
fn get_request_element(request: &'a str, foo: &'b str) -> Option<(&'a str, &'a str)> { ... }
```

Ok great, but what does this have to do with the error that we have ?

Since the compiler gives implicit lifetimes to function parameters (see **4.**), it means that in **1.**, an implicit lifetime has been allocated to the `value` parameter. This lifetime conflicts with the `'buffer` lifetime of the `impl` block.

In order to fix this problem, we should explicitely specify a lifetime to the `value: &'buffer str` keyword.

```rust
impl<'buffer> TryFrom<&'buffer [u8]> for Request<'buffer> {
    type Error = ParseError;

    fn try_from(value: &'buffer [u8]) -> Result<Self, Self::Error> {
    	// do stuff
    }
}
```

#### Query String

The problem with implementing the query string as a pointer to a String is that it would be painful to parse. Instead we can use HashMaps. As a Python developper, I prefer the name dictionnary.

To do this we can make use of the `std::collections::HashMap` structure that is being defined over two generics: the key and the value.

While it is pretty logic that the key will be a `&str` (careful lifetime), it is not so sure about the value. This could be a string, a bool or even a list (array).

This forces us to use a _heap allocated array_ as we don't know the length of the array at compile time. In Rust this is called a `Vector`

### Response

I needed to make a choice as to if I was going to implement all the HTTP status codes. Since this doesn't really serve any purpose for _learning Rust_, I kept it to a minimum and only chose:
- 200 OK
- 400 Bad Request
- 404 Not Found
- 500 Server Error

I first started writing to the `TcpStream` using the `write!` macro.

```rust
match stream.read(&mut buffer) {
    Ok(_) => {
        println!("Received request: {}", String::from_utf8_lossy(&buffer));
        match Request::try_from(&mut buffer as &[u8]) {
            Ok(request) => {

            	// HERE -----------------------------------------
                write!(stream, "HTTP/1.1 404 Not Found\r\n\r\n");
                // ----------------------------------------------

            },
            Err(err) => println!("{}", err)
        }
    },
    Err(err) => println!("Failed to read from connection: {}", err),
}
```

This sends a 404 to the browser. Cool!

#### Making it dynamic

Now I need to automate the response as I don't want it to be static. This means I need to pull the response from the `Response` struct.

This implies the implementation of the `Display` trait for `Response`.

```rust
match stream.read(&mut buffer) {
    Ok(_) => {
        println!("Received request: {}", String::from_utf8_lossy(&buffer));
        match Request::try_from(&mut buffer as &[u8]) {
            Ok(request) => {

            	// HERE -----------------------------------------
                let response = Response::new(
                	StatusCode::Ok,
                	Some("<h1>Hello World</h1>".to_string())
                );
                write!(stream, "{}", response);
                // ----------------------------------------------

            },
            Err(err) => println!("{}", err)
        }
    },
    Err(err) => println!("Failed to read from connection: {}", err),
}
```

#### Optimizing memory

While this approach is cool, let's take a step back and look at what is happening.

The `write!` macro sends the `Response` to the `TcpStream`. When it does that, it will convert the `Response` to a `String` using the `Display` trait.<br/>
This means that the `Response` is saved twice in the Heap. Once as a `Response` and once as a `String`. This behaviour could be problematic for a large body (think of the .js of react applications).

It would be more efficient to write the response directly to the stream without allocating a new `String`.

To do this we need to move the `write!` response to the `Response`.

```rust
// THE SERVER
match stream.read(&mut buffer) {
    Ok(_) => {
        println!("Received request: {}", String::from_utf8_lossy(&buffer));
        match Request::try_from(&mut buffer as &[u8]) {
            Ok(request) => {

            	// HERE -----------------------------------------
                let response = Response::new(
                	StatusCode::Ok,
                	Some("<h1>Hello World</h1>".to_string())
                );
                response.send(&mut stream);
                // ----------------------------------------------

            },
            Err(err) => println!("{}", err)
        }
    },
    Err(err) => println!("Failed to read from connection: {}", err),
}

// THE RESPONSE
impl Response {
    pub fn new(status_code: StatusCode, body: Option<String>) -> Self {
        Self { status_code, body }
    }

    pub fn send(&self, stream: &mut TcpStream) -> IoResult<()>{
        let body = match &self.body {
            Some(b) => b,
            None => ""
        };

        write!(
            stream,
            "HTTP/1.1 {} {}\r\n\r\n{}",
            self.status_code,
            self.status_code.verbose_status(),
            body,
        )
    }
}
```

#### Optimizing code

The last thing we want to do is to optimize the return of the `Response` in our server to prevent code duplication due to error handling.

To do this we assign the `Response` match to a variable and use an `if let` statement.

```rust
match stream.read(&mut buffer) {
    Ok(_) => {
        println!("Received request: {}", String::from_utf8_lossy(&buffer));

        // HERE -----------------------------------------
        let response = match Request::try_from(&mut buffer as &[u8]) {
            Ok(request) => {
                Response::new(
                    StatusCode::Ok,
                    Some("<h1>Hello World</h1>".to_string())
                )
            },
            Err(err) => {
                Response::new(
                    StatusCode::BadRequest,
                    Some("<h4>The request is invalid.</h4><br/><p>Please reformulate a valid request</p>".to_string())
                )
            }
        }

        if let Err(err) = response.send(&mut stream) {
            println!("Failed to send response: {}", err);
        }
        // ----------------------------------------------
    },
    Err(err) => println!("Failed to read from connection: {}", err),
}
```

### Custom Response Handler

The custom handler is actually a custom trait:

```rust
pub trait Handler {
    fn handle_request(&mut self, request: &Request) -> Response;
    fn handle_bad_request(&mut self, error: &ParseError) -> Response {
        println!("Failed to parse Request: {}", error);
        Response::new(
            StatusCode::BadRequest,
            Some("<h4>The request is invalid.</h4><br/><p>Please reformulate a valid request</p>".to_string())
        )
    }
}

impl WebServer {
	// ...

	pub fn run(self, mut handler: impl Handler) {
		// ...

		match stream.read(&mut buffer) {
            Ok(_) => {
                println!("Received request: {}", String::from_utf8_lossy(&buffer));
                let response = match Request::try_from(&mut buffer as &[u8]) {
                    Ok(request) => handler.handle_request(&request),
                    Err(err) => handler.handle_bad_request(&err)
                };

                if let Err(err) = response.send(&mut stream) {
                    println!("Failed to send response: {}", err);
                }
            },
            Err(err) => println!("Failed to read from connection: {}", err),
        }

        // ...
	}
}
```

This, of course implies that the call to the `run()` method in the `main.rs` file is passed a Handler.

```rust
// Run our server
server.run(WebsiteHandler);
```

And this in turn requires the existance of a `WebsiteHandler`:

```rust
use super::server::Handler;
use super::http::{Request, Response, StatusCode};


pub struct WebsiteHandler;


impl Handler for WebsiteHandler {
	fn handle_request(&mut self, request: &Request) -> Response {
		Response::new(StatusCode::Ok, Some("<h1>Hello world</h1>".to_string()))
	}
}
```

## Extra tips

### Implementors

When we go to the documentation of the `std` library (for example, the [read](https://doc.rust-lang.org/std/io/trait.Read.html) trait), we can see all `struct` from the `std` library that have this trait implementent by clicking on [`implementors`](https://doc.rust-lang.org/std/io/trait.Read.html#implementors).


### Display Trait

The [`Display`](https://doc.rust-lang.org/std/fmt/trait.Display.html) trait implements the `ToString()` for the type.

This basically means that implementing this trait allows to create a string return of a struct with us controlling how this string is created / composed.

### Conversion between `Result<T, E>` and `Option<T>`

The `Result` type has a method called `ok()`. This will look at the value of the result. It it was an `Ok`, it will take the value of the `Ok` and convert it in a `Option::Some(value)` of the same value. Otherwise it will convert it into a `Option::None`. 

### Muting warnings

`#![allow(dead_code)]`