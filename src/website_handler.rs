use std::fs;
use super::server::Handler;
use super::http::{Method, Request, Response, StatusCode};


pub struct WebsiteHandler {
    public_path: String,
}


impl WebsiteHandler {
    pub fn new(public_path: String) -> Self {
        Self { public_path }
    }

    fn read_file(&self, path: &str) -> Option<String> {
        // Get public path and append file path
        let absolute_path = format!("{}/{}", self.public_path, path);

        // Ensure that the absolute_path is part of the public directory
        match fs::canonicalize(absolute_path) {
            Ok(file_path) => {
                if file_path.starts_with(&self.public_path) {
                    // Read file to string
                    fs::read_to_string(file_path).ok()
                } else {
                    println!("Directory Traversal Attack Attempted: {}", path);
                    None
                }
            },
            Err(_) => None
        }
    }
}


impl Handler for WebsiteHandler {
    fn handle_request(&mut self, request: &Request) -> Response {
        match request.method() {
            Method::GET => match request.path() {
                "/" => Response::new(StatusCode::Ok, self.read_file("index.html")),
                "/foo" => Response::new(StatusCode::Ok, Some("<h1>Let's FOO</h1>".to_string())),
                "/bar" => Response::new(StatusCode::Ok, Some("<h1>Go BAR</h1>".to_string())),
                path => match self.read_file(path) {
                    Some(content) => Response::new(StatusCode::Ok, Some(content)),
                    None => Response::new(
                        StatusCode::NotFound,
                        Some("<h4>Oops ! could not find what you are looking for</h4>".to_string())
                    )
                }

            }
            _ => Response::new(
                StatusCode::NotFound,
                Some("<h4>Oops ! could not find what you are looking for</h4>".to_string())
            )
        }
    }
}