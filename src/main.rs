#![allow(dead_code)]

use std::env;
use server::WebServer;
use website_handler::WebsiteHandler;
use http::Method;
use http::Request;

mod server;
mod website_handler;
mod http;


fn main() {
    // Get environment variables
    let default_path = format!("{}/public", env!("CARGO_MANIFEST_DIR"));
    let public_path = env::var("PUBLIC_PATH").unwrap_or(default_path);
    println!("Public directory path: {}", public_path);

    // Create a new instance of our Web_Server
    let server = WebServer::new("127.0.0.1:7070".to_string());

    // Run our server
    server.run(WebsiteHandler::new(public_path));
}