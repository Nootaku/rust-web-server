use std::net::{TcpListener, TcpStream};
use std::io::{Read, Write};
use std::convert::TryFrom;
use crate::http::{ParseError, Request, Response, StatusCode};


pub trait Handler {
    fn handle_request(&mut self, request: &Request) -> Response;
    fn handle_bad_request(&mut self, error: &ParseError) -> Response {
        println!("Failed to parse Request: {}", error);
        Response::new(
            StatusCode::BadRequest,
            Some("<h4>The request is invalid.</h4><br/><p>Please reformulate a valid request</p>".to_string())
        )
    }
}


pub struct WebServer {
    addr: String,
}

impl WebServer {
    // I want to create a new instance of a WebServer
    pub fn new(addr: String) -> Self {
        Self {
            addr: addr,
        }
    }

    // Method
    pub fn run(self, mut handler: impl Handler) {
        // https://doc.rust-lang.org/std/net/struct.TcpListener.html#method.bind
        let tcp_listener = TcpListener::bind(&self.addr).unwrap();
        println!("Web Server listening on {}", self.addr);

        // loop for listening to new connexions
        loop {
            // https://doc.rust-lang.org/std/net/struct.TcpListener.html#method.accept
            match tcp_listener.accept() {
                Err(err) => println!("Failed to accept connection: {}", err),
                Ok((mut stream, _)) => {
                    // https://doc.rust-lang.org/std/io/trait.Read.html#tymethod.read
                    let mut buffer = [0; 1024];
                    match stream.read(&mut buffer) {
                        Ok(_) => {
                            println!("Received request: {}", String::from_utf8_lossy(&buffer));
                            let response = match Request::try_from(&mut buffer as &[u8]) {
                                Ok(request) => handler.handle_request(&request),
                                Err(err) => handler.handle_bad_request(&err)
                            };

                            if let Err(err) = response.send(&mut stream) {
                                println!("Failed to send response: {}", err);
                            }
                        },
                        Err(err) => println!("Failed to read from connection: {}", err),
                    }
                }
            }
        }
    }
}
