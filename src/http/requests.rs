use super::method::{Method, MethodError};
use super::QueryString;
use std::str;
use std::str::Utf8Error;
use std::convert::TryFrom;
use std::fmt::{Display, Debug, Formatter, Result as FmtResult};
use std::error::Error;


#[derive(Debug)]
pub struct Request<'buffer> {
    path: &'buffer str,
    query: Option<QueryString<'buffer>>,
    method: Method,
}


// create GETTERS for the request
impl <'buffer> Request<'buffer> {
    pub fn path(&self) -> &str {
        &self.path
    }

    pub fn method(&self) -> &Method {
        &self.method
    }

    pub fn query(&self) -> Option<&QueryString> {
        self.query.as_ref()
    }
}

// https://doc.rust-lang.org/std/convert/trait.TryFrom.html
impl<'buffer> TryFrom<&'buffer [u8]> for Request<'buffer> {
    type Error = ParseError;

    fn try_from(value: &'buffer [u8]) -> Result<Self, Self::Error> {
        let request = str::from_utf8(value)?;

        // Use function with variable shadowing
        let (method, request) = get_request_element(&request).ok_or(ParseError::InvalidRequest)?;
        let (mut path, request) = get_request_element(&request).ok_or(ParseError::InvalidRequest)?;
        let (protocol, _) = get_request_element(&request).ok_or(ParseError::InvalidRequest)?;

        // Assert valid protocol
        if protocol != "HTTP/1.1" {
            return Err(ParseError::InvalidProtocol);
        }

        // Set request method
        let method: Method = method.parse()?;

        // Set request path / query
        let mut query_string = None;

        if let Some(i) = path.find("?") {
            // Since query_string is currently None, we want to give it an
            // Option<Some> type
            query_string = Some(QueryString::from(&path[i + 1..]));
            path =  &path[..i];
        }

        // We need to return a Result<Self, Self::Error>
        // The Self::Error is handled above
        // The Result<Self> should be wrapped in the 'Ok()' statement
        Ok(Self {
            path: path,
            query: query_string,
            method: method,
        })
    }
}


// Helper functions ----------------------------------------------------------
fn get_request_element(request: &str) -> Option<(&str, &str)> {
    /*
        This function parses a string slice and returns a tuple containing the
        the first word of the string slice and the rest of the string slice 
        respectively.
        A word is defined by the presence of a space or a return carriage '\r'
        If the parsing is not possible or if there are no spaces, the result
        will be None.
    */
    for (index, character) in request.chars().enumerate() {
        if character == ' ' || character == '\r' {
            return Some((&request[..index], &request[index + 1..]));
        }
    }
    None
}


// Error handling ------------------------------------------------------------
pub enum ParseError {
    InvalidRequest,
    InvalidEncoding,
    InvalidProtocol,
    InvalidMethod,
}

impl ParseError {
    fn message(&self) -> &str {
        match self {
            Self::InvalidRequest => "InvalidRequest",
            Self::InvalidEncoding => "InvalidEncoding",
            Self::InvalidProtocol => "InvalidProtocol",
            Self::InvalidMethod => "InvalidMethod",
        }
    }
}

// Implement converion handlers for the ParseError
impl From<Utf8Error> for ParseError {
    fn from(_: Utf8Error) ->  Self {
        Self::InvalidEncoding
    }
}

impl From<MethodError> for ParseError {
    fn from(_: MethodError) ->  Self {
        Self::InvalidMethod
    }
}

impl Display for ParseError {
    fn fmt(&self, f: &mut Formatter<'_>) -> FmtResult {
        write!(f, "{}", self.message())
    }
}

impl Debug for ParseError {

    fn fmt(&self, f: &mut Formatter<'_>) -> FmtResult {
        write!(f, "{}", self.message())
    }
}

impl Error for ParseError {}