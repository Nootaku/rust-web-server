use std::collections::HashMap;


// documentation:
//     https://doc.rust-lang.org/std/collections/struct.HashMap.html
#[derive(Debug)]
pub struct QueryString<'buffer> {
	data: HashMap<&'buffer str, Value<'buffer>>,
}


#[derive(Debug)]
pub enum Value<'buffer> {
	Single(&'buffer str),
	Multiple(Vec<&'buffer str>),
}

impl <'buffer> QueryString<'buffer> {
	pub fn get(&self, key: &str) -> Option<&Value> {
		self.data.get(key)
	}
}

// This conversion cannot fail (any string can become a query string)
// Also TryFrom does not allow lifetimes
impl <'buffer> From<&'buffer str> for QueryString<'buffer> {
	fn from(s: &'buffer str) -> Self {
		// Create emtpy HashMap that we will fill
		let mut data = HashMap::new();

		// Split string on "&" character
		for element in s.split('&') {
			// to handle the case where the query param has no value (bool)
			let mut key = element;
			let mut value = "";

			// if the element has a value
			if let Some(i) = element.find('=') {
				key = &element[..i];
				value = &element[i + 1..];
			}

			// ---------------------------------------------------------------
			// possibility 1: the value is of type Single
			// possibility 2: the value is of type Single and already exists
			// possibility 3: the value is of type Multiple
			// ---------------------------------------------------------------

			// documentation:
			//     https://doc.rust-lang.org/std/collections/struct.HashMap.html#method.entry
			data.entry(key)
				.and_modify(|existing_value: &mut Value| match existing_value {
					Value::Single(old_value) => {
						let mut new_value_vector = vec![old_value, value];
						*existing_value = Value::Multiple(new_value_vector);
					},
					Value::Multiple(value_vector) => value_vector.push(value),
				})
				.or_insert(Value::Single(value)); // insert key with given value if it does not exist
			
		}
		
		QueryString {
			data: data
		}
	}
}