use std::fmt::{Display, Formatter, Result as FmtResult};


#[derive(Copy, Clone, Debug)]
pub enum StatusCode {
	Ok = 200,
	BadRequest = 400,
	NotFound = 404,
	ServerError = 500
}


impl StatusCode {
	pub fn verbose_status(&self) -> &str {
		match self {
			Self::Ok => "Ok",
			Self::BadRequest => "Bad Request",
			Self::NotFound => "Not Found",
			Self::ServerError => "Server Error",
		}
	}
}


impl Display for StatusCode {
	fn fmt(&self, formatter: &mut Formatter) -> FmtResult {
		// https://doc.rust-lang.org/error_codes/E0606.html
		write!(formatter, "{}", *self as u16)
	}
}
